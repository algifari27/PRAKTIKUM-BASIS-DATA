# 1 
Mampu mendemonstrasikan perancangan basis data berdasarkan permasalahan dunia nyata melalui reverse engineering produk digital global. Lampirkan bukti tabel hasil desain
![](https://gitlab.com/tugas-praktikum-basis-data/tugas-prak-basis-data/-/raw/main/Desain_gambar.png)

# 2
Mampu mendemonstrasikan DATA DEFINITION LANGUAGE (DDL) secara tepat berdasarkan minimal 10 entitas dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record
![](https://gitlab.com/tugas-praktikum-basis-data/tugas-prak-basis-data/-/raw/main/DDL.gif)

# 3
Mampu mendemonstrasikan DATA MANIPULATION LANGUAGE (DML) berdaraskan minimal 5 use case operasional (CRUD) dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record
![](https://gitlab.com/tugas-praktikum-basis-data/tugas-prak-basis-data/-/raw/main/DML.gif)

# 4
Mampu mendemonstrasikan DATA QUERY LANGUAGE (DQL) berdaraskan minimal 5 pertanyaan analisis dari produk digital yang dipilih, menggunakan setidaknya keyword GROUP BY, INNER JOIN, LEFT / RIGHT JOIN, AVERAGE / MAX / MIN. Lampirkan bukti berupa source code dan screen record.
![](https://gitlab.com/tugas-praktikum-basis-data/tugas-prak-basis-data/-/raw/main/DQL.gif)

# 5
Mampu mendemonstrasikan keseluruhan DDL, DML, dan DQL dari produk digital yang dipilih dalam bentuk video publik di Youtube. Lampirkan link Youtube
[![Watch the video]](https://youtu.be/OcVBpGyowjE)
